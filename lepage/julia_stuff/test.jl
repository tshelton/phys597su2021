### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 3196098c-dd0f-11eb-334c-bb3a4280b45c
begin
	using Plots
end

# ╔═╡ 5f9264fb-fbd5-4336-b914-17480be91655
struct Params{T<:AbstractFloat,S<:Integer}
    a::T # Lattice spacing
    ϵ::T # Uniform distribution range
    N::S # Number of sites
    N_cor::S # Number of steps to de-correlate random paths
    N_cf::S # Number of configurations (random paths)
    action::S # Which action are we using.
end

# ╔═╡ 7132d478-2a13-4295-890e-b41ce64f1c38
a = 0.5; N = 20; ϵ=1.4; N_cor=20; N_cf = trunc(Int,1e6); action=1;

# ╔═╡ bc6d9163-1f07-4353-9f60-7880d4741e1f
params = Params(a,ϵ,N,N_cor,N_cf,action)

# ╔═╡ 4dbeb1c8-440a-45e8-ad48-4b48b0dd8427
params.a

# ╔═╡ Cell order:
# ╠═3196098c-dd0f-11eb-334c-bb3a4280b45c
# ╠═5f9264fb-fbd5-4336-b914-17480be91655
# ╠═7132d478-2a13-4295-890e-b41ce64f1c38
# ╠═bc6d9163-1f07-4353-9f60-7880d4741e1f
# ╠═4dbeb1c8-440a-45e8-ad48-4b48b0dd8427
